import android.app.Activity;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class MainActivity extends Activity {

@Override
protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    this.setContentView(R.layout.main);
    String url = "https://elbinario.net";
    WebView myWebView = (WebView) this.findViewById(R.id.webView);
    myWebView = (WebView) findViewById(R.id.webView);
    // Configure related browser settings
    myWebView.getSettings().setLoadsImagesAutomatically(true);
    myWebView.getSettings().setJavaScriptEnabled(true);
    myWebView.getSettings().setDomStorageEnabled(true);
    myWebView.loadUrl(url);
    myWebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_INSET);
    // Configure the client to use when opening URLs
    myWebView.setWebViewClient(new MyWebViewClient());



}

private class MyWebViewClient extends WebViewClient {

    @Override
    public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
        handler.proceed();
    }

    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        view.loadUrl(url);
        return true;

    }

}

} 